<?php
// include and register Twig auto-loader
require_once 'vendor/twig/twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

try {
  // specify where to look for templates
  $loader = new Twig_Loader_Filesystem('views/');
  
  // initialize Twig environment
  $twig = new Twig_Environment($loader);
  
  // load template
  $template = $twig->loadTemplate('saludo.html');
  
  // set template variables
  // render template
  echo $template->render(array(
    'nombre' => 'Luis Miguel',
    'usuario' => 'lmm',
    'password' => 'Pyth0nPower',
  ));
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}
?>