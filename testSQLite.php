<?php
// variables
$crear_tabla = 'CREATE TABLE IF NOT EXISTS empleado(
id INTEGER PRIMARY KEY,
nombre TEXT,
email TEXT
)';
$datos = array(
			array('nombre'=>"Luis Miguel",
					'email'=>"luis@luis.com"),
			array('nombre'=>"Pilar",
			       'email'=> "pilar@dezaragoza.com")
)



?>


<?php
// test de acceso a sqlite
try {
	// crear bases de datos
	$conn = new PDO('sqlite:empleados.db');
	//$conn = new PDO('sqlite::memory:'); //en memoria
	// creación de la tabla
	$conn->exec($crear_tabla);
	// insertar datos
	// 1. preparar sentencia de inserción
	$insertar = "INSERT into empleado(nombre, email)
				VALUES(:nombre, :email)";
	$sentencia = $conn->prepare($insertar);
	
	foreach($datos as $emp){
		$sentencia->execute($emp);
		echo "Insertado ", $emp['nombre'], '<br>';
	}
	// select
	$listado = 'Select nombre, email from empleado';
	$resultado = $conn->query($listado);
	foreach($resultado as $emp){
		echo 'Nombre: <strong>', $emp['nombre'], '</strong><br>';
		echo 'Email: <strong>', $emp['email'], '</strong><br>';
		echo '<br>';
	}
	
	
} catch(PDOException $e){
	echo $e->getMessage();
}

// cierra conexion
$conn = null;

?>