<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>Índice de masa corporal (Formulario). Operaciones aritméticas.
  Ejercicios. PHP. Bartolomé Sintes Marco</title>
  <meta name="generator" content="amaya 8.7.1, see http://www.w3.org/Amaya/" />
  <link href="mclibre_php_soluciones.css" rel="stylesheet" type="text/css"
  title="Color" />
</head>

<body>
<h1>Índice de masa corporal (Formulario)</h1>

<form action="masacorporal.php" method="get">
  <fieldset>
    <legend>Formulario</legend>
    <p>Escriba su peso en kilogramos y su altura en centímetros para calcular 
    su índice de masa corporal.</p>

    <table cellspacing="5" class="borde">
      <tbody>
        <tr>
          <td><strong>Peso:</strong></td>
          <td><input type="text" name="peso" size="5" maxlength="5" /> kg</td>
        </tr>
        <tr>
          <td><strong>Altura:</strong></td>
          <td><input type="text" name="altura" size="5" maxlength="5" /> cm</td>
        </tr>
      </tbody>
    </table>

    <p class="der">
    <input type="submit" value="Calcular" /> 
    <input type="reset" value="Borrar" name="Reset" /></p>
  </fieldset>
</form>

<?php
if (isset($_GET['altura'])){
  include("masa_corporal.php");
  $altura = $_GET['altura'];
  $peso = $_GET['peso'];

printf ("<p>Indice de masa corporal %.2f</p>", 
	imc($peso, $altura));
}
?>


</body>
</html>
